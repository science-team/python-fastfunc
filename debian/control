Source: python-fastfunc
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper (>= 11),
 dh-python, python3-all-dev, python3-setuptools,
 python3-pybind11
Standards-Version: 4.2.1
Homepage: https://github.com/nschloe/fastfunc
Vcs-Browser: https://salsa.debian.org/science-team/python-fastfunc
Vcs-Git: https://salsa.debian.org/science-team/python-fastfunc.git

Package: python3-fastfunc
Architecture: any
Depends: ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}
Description: Fast numpy ufunc operations (Python 3)
 NumPy's own ufunc operations are handy, but can be quite slow
 (see https://github.com/numpy/numpy/issues/5922 and
 https://github.com/numpy/numpy/issues/11156).
 Use fastfunc as a drop-in replacement
 .
    # import numpy
    # numpy.add.at(target, idx, vals)
    import fastfunc
    fastfunc.add.at(target, idx, vals)
 .
 to get a speed-up by a factor of 40.
 .
 This is achieved by moving the operations to C++ using pybind11.
 .
 This package installs the library for Python 3.
